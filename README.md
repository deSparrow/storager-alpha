# Storager-Alpha #
Jestem inżynierem po informatyce na Politechnice Białostockiej. Witaj w moim repozytorium. Zajmuję się
głównie tworzeniem aplikacji mobilnych oraz stron internetowych. Jeśli chcesz się ze
mną skontaktować, użyj jednego z poniższych adresów:

* sparrowhawk1076[at]gmail.com
* work.f.olszewski[at]gmail.com.

## Storager ##
Storager jest mini platformą wspierającą wykonywanie inwentaryzacji, umożliwiając przechowywanie
produktów, skanowanie kodów kreskowych itd. Projekt ten wykonałem na obronę pracy inżynierskiej.

## Strona internetowa ##
Obecnie istnieje [strona](https://storager.000webhostapp.com) internetowa systemu Storager, na
której można założyć konto, przeglądać produkty czy pobrać arkusz z danymi.

Lista produktów: |
-----|
![Img3.png](http://gdurl.com/dHDw) |
	
## Aplikacja mobilna ##
Po założeniu konta na stronie, możliwe jest zalogowanie się w aplikacji mobilnej, gdzie da się
zarządzać produktami, skanować kody kreskowe i wykonywać zdjęcia produktom. Plik .apk
można pobrać na tym repozytorium.

Ekran logowania: | Aktywność z produktami:  
-----| ---- 
![Img3.png](http://gdurl.com/cf03) | ![Img4.png](http://gdurl.com/c3PH)
	
## Serwis internetowy ##
Jednym z elementów systemu Storager jest publiczny webservice o architekturze typu REST. Jego opis
jest dostępny [tutaj](https://storager.000webhostapp.com/api.php). Dane o produktach znajdują się
w zdalnej bazie danych na serwerze.
